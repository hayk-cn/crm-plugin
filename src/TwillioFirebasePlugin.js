import { FlexPlugin } from 'flex-plugin'
import React from 'react'
import CustomTaskListComponent from './CustomTaskListComponent'

const PLUGIN_NAME = 'TwillioFirebasePlugin'

var audioElement

function playSound () {
  let notificationAudio = document.createElement('AUDIO')
  notificationAudio.style.display = 'none'
  if (notificationAudio.canPlayType('audio/mpeg')) {
    notificationAudio.setAttribute('src',
      'https://res.cloudinary.com/dnekxz4te/video/upload/v1557095450/remix_adr8nu.mp3')
  } else {
    notificationAudio.setAttribute('src',
      'https://res.cloudinary.com/dnekxz4te/video/upload/v1557095450/remix_adr8nu.mp3')
  }

  notificationAudio.setAttribute('controls', 'controls')
  document.body.appendChild(notificationAudio)
  notificationAudio.play()
  audioElement = notificationAudio
}

function stopSound () {
  console.log('AUDIO ELEMENT')
  console.log(audioElement)
  audioElement.pause()
}

function notifyMe () {

  // Let's check if the browser supports notifications
  if (!('Notification' in window)) {
    alert('This browser does not support desktop notification')
  }

  // Let's check whether notification permissions have already been granted
  else if (Notification.permission === 'granted') {
    // If it's okay let's create a notification
    var notification = new Notification('You have incoming call!')
    notification.onclick = function (event) {
      event.preventDefault() // prevent the browser from focusing the Notification's tab
      window.open('https://flex.twilio.com/agent-desktop/', '_blank')
    }
    notification.onclose = function () { stopSound() }
    playSound()
  }

  // Otherwise, we need to ask the user for permission
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission().then(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === 'granted') {
        var notification = new Notification('You have incoming call!')
        playSound()
      }
    })
  }

  // At last, if the user has denied notifications, and you
  // want to be respectful there is no need to bother them any more.
}

Notification.requestPermission().then(function (result) {
  console.log(result)
})

function spawnNotification (body, icon, title) {
  var options = {
    body: body,
    icon: icon,
    vibrate: [100, 200, 100],
  }
  var n = new Notification(title, options)
}

export default class TwillioFirebasePlugin extends FlexPlugin {
  constructor () {
    super(PLUGIN_NAME)
  }

  /**
   * This code is run when your plugin is being started
   * Use this to modify any UI components or attach to the actions framework
   *
   * @param flex { typeof import('@twilio/flex-ui') }
   * @param manager { import('@twilio/flex-ui').Manager }
   */
  init (flex, manager) {
    // flex.AgentDesktopView.Panel1.Content.add(
    //   <CustomTaskListComponent key="demo-component" />,
    //   {
    //     sortOrder: -1,
    //   }
    // );

    manager.workerClient.on('reservationCreated', reservation => {
      fetch(
        'https://aureolin-zorse-8830.twil.io/list-reservations?workspaceSid=' +
        manager.workerClient._routes.workspaceSid + '&workerSid=' +
        manager.workerClient._routes.workerSid).
        then(res => res.json()).
        then(data => {
          const pendingCalls = data.filter(
            (call) => call.status === 'pending' || call.status === 'accepted')
          if (pendingCalls.length > 1) {} else {
            notifyMe()
          }
        }).
        catch(e => {
          console.error(e)
        })
      reservation.on('accepted', () => {
        stopSound()
        console.log('stop sound')
      })
      reservation.on('rejected', function (reservation) {
        stopSound()
        console.log('rejected')                  // WRxxx
      })
    })

    // manager.workerClient.on("reservationAccepted", reservation => {
    //
    // });

    flex.MainHeader.defaultProps.logoUrl = 'http://www.cloudnetworks.tech/wp-content/uploads/2017/07/logo_463pxX271px_website.png'
    flex.CRMContainer.defaultProps.uriCallback = (task) => {
      console.log('Log Task Info!!', task)
      return task ? 'https://cnstwilio.firebaseapp.com/task-data/' +
        task.attributes.caller : 'https://cnstwilio.firebaseapp.com/'
    }
  }
}
