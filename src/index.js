import * as FlexPlugin from 'flex-plugin';
import TwillioFirebasePlugin from './TwillioFirebasePlugin';

FlexPlugin.loadPlugin(TwillioFirebasePlugin);
